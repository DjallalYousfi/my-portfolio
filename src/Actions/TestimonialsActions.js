// export const addTestimonial = (testimonial) => {
//     return {
//       type: 'ADD_TESTIMONIAL',
//       payload: testimonial,
//     };
//   };


  // Action types
export const ADD_TESTIMONIAL = 'ADD_TESTIMONIAL';
export const DELETE_TESTIMONIAL = 'DELETE_TESTIMONIAL';
export const UPDATE_TESTIMONIAL = 'UPDATE_TESTIMONIAL';


export const addTestimonial = (testimonial) => ({
    type: 'ADD_TESTIMONIAL',
    payload: testimonial,
  });
  
  export const deleteTestimonial = (index) => ({
    type: 'DELETE_TESTIMONIAL',
    payload: index,
  });
  
  export const updateTestimonial = (index, testimonial) => ({
    type: 'UPDATE_TESTIMONIAL',
    payload: { index, testimonial },
  });