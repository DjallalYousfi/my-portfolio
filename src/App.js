import "./App.css";
import Home from "./components/Home";
import Projects from "./components/Projects";
import Contact from "./components/Contact";
import Header from "./components/Header";
import Footer from "./components/Footer";
import AboutMe from "./components/AboutMe";
import Testimonials from "./components/Testimonials";
import "@fortawesome/fontawesome-free/css/all.min.css";
import Formation from "./components/Formation";
import { Route, Routes } from "react-router-dom";
import FirstProject from "./components/FirstProject";
import SecondProject from "./components/SecondProject";
// import ListTestimonials from './components/ListTestimonials';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/aboutme" element={<AboutMe />} />
        <Route path="/formation" element={<Formation />} />
        <Route path="/projects" element={<Projects />} />
        <Route path="/firstproject" element={<FirstProject />} />
        <Route path="/secondproject" element={<SecondProject />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/testimonials" element={<Testimonials />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
