import React from "react";
import "./AboutMe.modules.css";

function AboutMe() {
  return (
    <div className="aboutme">
      <h2>About Me</h2>

      <div className="paragraphe">
        <p>
          &nbsp; &nbsp; As a computer programmer, I am passionate about coding
          and solving complex problems through software development. With a
          strong foundation in programming languages such as JavaScript, HTML,
          and CSS, I have experience in building web applications and creating
          responsive and user-friendly interfaces. I am skilled in various
          frameworks and libraries, including React, Node.js, and Express, which
          allow me to develop robust and scalable applications. I have
          experience working with databases like MySQL and MongoDB, enabling me
          to design efficient data storage and retrieval systems. Continuous
          learning is an essential part of my journey as a programmer, and I
          stay updated with the latest industry trends and technologies. I enjoy
          collaborating with teams, and my effective communication and
          problem-solving skills contribute to successful project delivery. With
          a passion for innovation and attention to detail, I strive to deliver
          high-quality code and create solutions that exceed expectations. I am
          dedicated to expanding my knowledge and skills to contribute to the
          success of future projects and make a positive impact in the field of
          software development.
        </p>
      </div>

      <div className="skills">
        {/* <h2>Skills</h2> */}
        <ul>
          <li>
            <span className="prog-lang">PROGRAMMING LANGUAGES:</span>
            <div className="imageContainer">
              <span className="java"> JAVA </span>
              <span className="cpp"> C++ </span>
              <span className="csh"> C# </span>
            </div>
          </li>
          <li>
            <span className="prog-lang"> WEB: </span>
            <div className="imageContainer">
              <span className="html"> HTML </span>
              <span className="css"> CSS </span>
              <span className="php"> PHP </span>
              <span className="javascript"> JavaScript </span>
            </div>
          </li>
          <li>
            <span className="prog-lang"> DATA BASES: </span>
            <div className="imageContainer">
              <span className="mysql"> MySQL </span>
              <span className="mongodb"> MongoDB </span>
              <span className="cassandra"> Cassandra </span>
              <span className="sqlserver"> SQL Server </span>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default AboutMe;
