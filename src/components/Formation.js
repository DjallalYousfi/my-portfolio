import React from "react";
import image1 from "../Images/La-Cite.jpg";
import image2 from "../Images/Univ_FAS.jpg";
import image3 from "../Images/Lycee.jpg";
import "./Formation.modules.css";

function Formation() {
  return (
    <>
      <div className="formation">
        <h2>Formation</h2>
      </div>
      <div className="form1">
        <ul>
          <li className="flex-container">
            <div className="formation-content">
              <div className="year">2022-2023</div>
              <div className="diploma">DEC in Computer Programming</div>
              <div className="location">
                College La Cite, Ottawa, ON, Canada
              </div>
            </div>
            <a href="https://www.collegelacite.ca/">
              <div>
                <img
                  src={image1}
                  alt="La cite collegiale"
                  className="img-formation"
                />
              </div>
            </a>
          </li>
        </ul>
      </div>
      <div className="form2">
        <ul>
          <li className="flex-container">
            <div className="formation-content">
              <div className="year">2017-2020</div>
              <div className="diploma">
                Diploma Licence in Computer Sciences
              </div>
              <div className="location">
                University Ferhat Abbas Setif, Algeria
              </div>
            </div>
            <a href="https://www.univ-setif.dz/">
              <div>
                <img
                  src={image2}
                  alt="Universite Ferhat Abbas Setif1"
                  className="img-formation"
                />
              </div>
            </a>
          </li>
        </ul>
      </div>
      <div className="form3">
        <ul>
          <li className="flex-container">
            <div className="formation-content">
              <div className="year">2017</div>
              <div className="diploma">
                Diploma BAC in Technical Mathematics - Electrical Engineering -
              </div>
              <div className="location">
                Lycee Nacerddine Nacer Draa-Kebila, Setif, Algeria
              </div>
            </div>
            <a href="https://www.vitaminedz.com/fr/Setif/lycee-de-draa-kebila-193109-Articles-19-132-1.html">
              <div>
                <img
                  src={image3}
                  alt="Lycee_Draa_Kebila Setif"
                  className="img-formation"
                />
              </div>
            </a>
          </li>
        </ul>
      </div>
    </>
  );
}

export default Formation;
