import React, { useState } from "react";
import './Contact.css'

function Contact() {
  // Regex pour les validations
  const emailRegex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
  const nameRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;

  const [state, setState] = useState({
    name: "",
    email: "",
    message: "",
  });

  // Erreurs de validation
  const [errors, setErrors] = useState({
    name: "",
    email: "",
    message: "",
  });

  function handleChange(event) {
    const { name, value } = event.target;
    validateField(name, value);
    setState((previousState) => ({ ...previousState, [name]: value }));
  }

  // Valider chaque champ avec les conditions données
  function validateField(field, value) {
    switch (field) {
      case "name":
        if (!nameRegex.test(value))
          setErrors((prev) => ({ ...prev, [field]: `${field} is not valid` }));
        else setErrors((prev) => ({ ...prev, [field]: "" }));
        break;
      case "email":
        if (!emailRegex.test(value))
          setErrors((prev) => ({ ...prev, [field]: `${field} is not valid` }));
        else setErrors((prev) => ({ ...prev, [field]: "" }));
        break;
      case "message":
        if (!value)
          setErrors((prev) => ({ ...prev, [field]: `${field} is required` }));
        else setErrors((prev) => ({ ...prev, [field]: "" }));
        break;
      default:
        break;
    }
  }

  // Vérifier si un champ a une erreur pour l'afficher
  function fieldHasError(field) {
    return errors[field] && errors[field] !== "";
  }

  // Vérifier si tout le formulaire est valide
  function isFormValid() {
    let isValid = true;
    Object.keys(state).forEach((field) => {
      validateField(field, state[field]);
      if (errors[field] && errors[field] !== "") {
        isValid = false;
      }
    });
    return isValid;
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (isFormValid()) {
      // Envoyer les données du formulaire
      console.log("Form submitted:", state);
    } else {
      console.log("Form has errors:", errors);
    }
  }

  return (
    <div className="contact">
      <section id="get-in-touch">
        <h2>Contact</h2>
        <p>
          Feel free to reach out to me if you have any questions or
          collaboration opportunities.
        </p>
        <form onSubmit={handleSubmit}>
          <div>
            <input
              className={`form-control ${
                fieldHasError("name") && "is-invalid"
              }`}
              value={state.name}
              onChange={handleChange}
              type="text"
              name="name"
              placeholder="Your Name"
            />
            <div
              className={
                fieldHasError("name") ? "invalid-feedback" : "valid-feedback"
              }
            >
              {errors.name}
            </div>
          </div>
          <div>
            <input
              className={`form-control ${
                fieldHasError("email") && "is-invalid"
              }`}
              value={state.email}
              onChange={handleChange}
              type="email"
              name="email"
              placeholder="Your Email"
            />
            <div
              className={
                fieldHasError("email") ? "invalid-feedback" : "valid-feedback"
              }
            >
              {errors.email}
            </div>
          </div>
          <div>
            <textarea
              className={`form-control ${
                fieldHasError("message") && "is-invalid"
              }`}
              value={state.message}
              onChange={handleChange}
              name="message"
              placeholder="Your Message"
            ></textarea>
            <div
              className={
                fieldHasError("message") ? "invalid-feedback" : "valid-feedback"
              }
            >
              {errors.message}
            </div>
          </div>
          <button type="submit">Send Message</button>
        </form>
      </section>
    </div>
  );
}

export default Contact;
