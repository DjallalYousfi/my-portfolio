import React, { useState } from "react";
import { connect } from "react-redux";
import {
  addTestimonial,
  deleteTestimonial,
  updateTestimonial,
} from "../Actions/TestimonialsActions";
import "./Testimonials.css";

function Testimonials({
  testimonials,
  addTestimonial,
  deleteTestimonial,
  updateTestimonial,
}) {
  const nameRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;

  const [state, setState] = useState({
    familyname: "",
    firstName: "",
    message: "",
  });

  const [errors, setErrors] = useState({
    familyname: "",
    firstName: "",
    message: "",
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
    validateField(name, value);
  };

  const validateField = (field, value) => {
    let isValid = true;
    let errorMessage = "";

    switch (field) {
      case "familyname":
      case "firstName":
        if (!nameRegex.test(value)) {
          isValid = false;
          errorMessage = `${field} n'est pas valide`;
        }
        break;
      case "message":
        if (!value) {
          isValid = false;
          errorMessage = "Note is required";
        }
        break;
      default:
        break;
    }

    setErrors((prevState) => ({
      ...prevState,
      [field]: errorMessage,
    }));

    return isValid;
  };

  const isFormValid = () => {
    let isValid = true;

    for (const field in state) {
      if (field !== "assiduite") {
        const value = state[field];
        const isValidField = validateField(field, value);
        if (!isValidField) {
          isValid = false;
        }
      }
    }

    return isValid;
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (isFormValid()) {
      const { familyname, firstName, message } = state;
      const newTestimonial = { familyname, firstName, message };

      if (editingIndex > -1) {
        updateTestimonial(editingIndex, newTestimonial);
        setEditingIndex(-1);
      } else {
        addTestimonial(newTestimonial);
      }

      setState({
        familyname: "",
        firstName: "",
        message: "",
      });
      setErrors({
        familyname: "",
        firstName: "",
        message: "",
      });
    }
  };

  const handleEdit = (index) => {
    const testimonial = testimonials[index];
    setState({
      familyname: testimonial.familyname,
      firstName: testimonial.firstName,
      message: testimonial.message,
    });
    setEditingIndex(index);
  };

  const handleDelete = (index) => {
    deleteTestimonial(index);
  };

  const fieldHasError = (field) => {
    return errors[field] !== "";
  };

  const { familyname, firstName, message } = state;
  const [editingIndex, setEditingIndex] = useState(-1);

  return (
    <div className="testimonials">
      <h2>Testimonials</h2>
      <div className="testimonial-list">
        {testimonials.length > 0 ? (
          testimonials.map((testimonial, index) => (
            <div className="testimonial" key={index}>
              <h3>
                {testimonial.familyname} {testimonial.firstName}
              </h3>
              <p>{testimonial.message}</p>
              <div className="testimonial-buttons">
                <button className="edit" onClick={() => handleEdit(index)}>
                  Edit
                </button>
                <button className="delete" onClick={() => handleDelete(index)}>
                  Delete
                </button>
              </div>
            </div>
          ))
        ) : (
          <p>No testimonials yet.</p>
        )}
      </div>
      <form onSubmit={handleSubmit} className="testimonial-form">
        <h3>Add a Testimonial</h3>
        <div className="form-group">
          <label htmlFor="name">Family Name:</label>
          <input
            type="text"
            id="name"
            name="familyname"
            value={familyname}
            onChange={handleChange}
            required
          />
          {fieldHasError("familyname") && (
            <p className="error">{errors.familyname}</p>
          )}
        </div>
        <div className="form-group">
          <label htmlFor="firstName">First Name:</label>
          <input
            type="text"
            id="firstName"
            name="firstName"
            value={firstName}
            onChange={handleChange}
            required
          />
          {fieldHasError("firstName") && (
            <p className="error">{errors.firstName}</p>
          )}
        </div>
        <div className="form-group">
          <label htmlFor="message">Note:</label>
          <textarea
            id="message"
            name="message"
            value={message}
            onChange={handleChange}
            required
          ></textarea>
          {fieldHasError("message") && (
            <p className="error">{errors.message}</p>
          )}
        </div>
        <button type="submit">{editingIndex > -1 ? "Update" : "Submit"}</button>
      </form>
    </div>
  );
}

const mapStateToProps = (state) => ({
  testimonials: state.testimonials,
});

const mapDispatchToProps = (dispatch) => ({
  addTestimonial: (testimonial) => dispatch(addTestimonial(testimonial)),
  deleteTestimonial: (index) => dispatch(deleteTestimonial(index)),
  updateTestimonial: (index, testimonial) =>
    dispatch(updateTestimonial(index, testimonial)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Testimonials);
