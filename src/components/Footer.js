import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLinkedin,
  faTwitter,
  faInstagram,
  faGitlab,
} from "@fortawesome/free-brands-svg-icons";
import "./Footer.modules.css";

function Footer() {
  return (
    <div className="footer">
      <footer>
        <div>Designed and developped by Djallal Yousfi</div>
        <div>&copy; Copyright 2023</div>
        <div className="social-media-footer">
          <a
            href="https://www.instagram.com/djalla_l.ysfi/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon icon={faInstagram} />
          </a>
          <a
            href="https://twitter.com/YousfiDjallal"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon icon={faTwitter} />
          </a>
          <a
            href="https://www.linkedin.com/in/djallal-yousfi-5845a9186/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon icon={faLinkedin} />
          </a>
          <a
            href="https://gitlab.com/DjallalYousfi/portfolio_partie1"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon icon={faGitlab} />
          </a>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
