import React from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import image1 from "../Images/EL_1.png";
import image2 from "../Images/EL_2.png";
import image3 from "../Images/EL_3.png";
import image4 from "../Images/EL_4.png";
import image5 from "../Images/EL_5.png";

function FirstProject() {
  const images = [
    { id: 1, src: image1 },
    { id: 2, src: image2 },
    { id: 3, src: image3 },
    { id: 4, src: image4 },
    { id: 5, src: image5 },
  ];
  return (
    <>
      <div className="proj1">
        <h1>Application web E-learning</h1>
      </div>

      <ul>
        <li>
          {/* <img src={image} alt="Projet1" className="img-projet1" /> */}
          {/* <span className='year'>2020</span> */}
          <p>
            &nbsp; &nbsp; &nbsp; &nbsp; L'objectif principal de ce projet est de
            concevoir et de développer une application web pour l'apprentissage
            en ligne, en utilisant des outils et des frameworks tels que Laravel
            et Bootstrap. À l'échelle mondiale, la technologie et l'enseignement
            à distance ont pris une importance considérable, ce qui nous a
            inspiré pour créer une plateforme d'apprentissage. Cette plateforme
            permettra aux apprenants et aux enseignants d'accomplir leur travail
            habituel depuis leur domicile, technologies: Laraval, Bootstrap.{" "}
            <br /> <br />
            &nbsp; &nbsp; &nbsp; &nbsp; Mots clés : Application Web, E-learning,
            UML, Framework Laravel, Bootstrap.
          </p>
        </li>
      </ul>
      <div>
        <Carousel>
          {images.map((image) => (
            <div key={image.id}>
              <img
                src={image.src}
                alt={`Projet ${image.id}`}
                className="img-projet"
              />
            </div>
          ))}
        </Carousel>
      </div>
      <div>
        <a
          href="PFE Application web E-learning.pdf"
          download="Djallal PFE Application web E-learning.pdf"
        >
          <button className="buttonRapport">Get Rapport of this project</button>{" "}
          <br /> <br />
        </a>
      </div>
    </>
  );
}

export default FirstProject;
