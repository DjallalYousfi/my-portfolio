import React from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import image1 from "../Images/Home.png";
import image2 from "../Images/Inscription.png";
import image3 from "../Images/Log in.png";

function SecondProject() {
  const images = [
    { id: 1, src: image1 },
    { id: 2, src: image2 },
    { id: 3, src: image3 },
  ];
  return (
    <div className="secondproj">
      <div className="proj1">
        <h2>
          Mise en place d'une plateforme d'envoi d'objets à l'étranger <br />
          Co-Luggage
        </h2>
      </div>

      <ul>
        <li>
          {/* <img src={image} alt="Projet1" className="img-projet1" /> */}
          {/* <span className='year'>2020</span> */}
          <p>
            &nbsp; &nbsp; &nbsp; &nbsp; Le projet vise à simplifier l'envoi
            international d'objets en mettant en relation les voyageurs et les
            expéditeurs. Notre objectif est de fournir une solution pratique et
            économique qui offre une alternative aux services d'expédition
            traditionnels. Nous voulons rendre le processus d'expédition plus
            accessible en permettant aux utilisateurs de trouver des voyageurs
            de confiance pour transporter leurs colis à l'étranger. En
            connectant les voyageurs et les expéditeurs, nous visons à réduire
            les coûts et les complications associés à l'envoi international,
            offrant ainsi une solution plus abordable et pratique pour tous les
            utilisateurs.", technologies: Node js, React js, MySQL <br /> <br />
            &nbsp; &nbsp; &nbsp; &nbsp; Mots clés : Application Web, Co-Luggage,
            UML, Nodejs, Reactjs, MySQL.
          </p>
        </li>
      </ul>
      <div>
        <Carousel>
          {images.map((image) => (
            <div key={image.id}>
              <img
                src={image.src}
                alt={`Projet ${image.id}`}
                className="img-projet"
              />
            </div>
          ))}
        </Carousel>
      </div>
      <div>
        <a href="Co-Luggage.pdf" download="Djallal Co-Luggage.pdf">
          <button className="buttonRapport">Get Rapport of this project</button>{" "}
          <br /> <br />
        </a>
      </div>
    </div>
  );
}

export default SecondProject;
