import React from "react";
import image from "../Images/Djallal.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLinkedin,
  faTwitter,
  faGitlab,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";
import "./Home.css";
import Typical from "react-typical";
import { NavLink } from "react-router-dom";

function Home() {
  return (
    <>
      <div className="home">
        <div className="text-container">
          <div className="color-changing">
            <h1>Welcome to my portfolio!</h1>
          </div>

          <div className="social-media">
            <a
              href="https://www.instagram.com/djalla_l.ysfi/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon icon={faInstagram} />
            </a>
            <a
              href="https://twitter.com/YousfiDjallal"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon icon={faTwitter} />
            </a>
            <a
              href="https://www.linkedin.com/in/djallal-yousfi-5845a9186/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon icon={faLinkedin} />
            </a>
            <a
              href="https://gitlab.com/DjallalYousfi/portfolio_partie1"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon icon={faGitlab} />
            </a>{" "}
            <br /> <br />
          </div>
          <div>
            <h2>
              Hello, I'm <span className="nameColor">Djallal Yousfi</span>{" "}
            </h2>
          </div>
          <div>
            <h3>Web Developer</h3>
          </div>
          <div className="home-resume">
            <p>
              I'm a passionate web developer with expertise in HTML, CSS,
              JavaScript, and PHP. I have a strong background in creating
              responsive and user-friendly web applications. My goal is to
              leverage my skills to develop innovative and efficient solutions
              that enhance the user experience.
            </p>
          </div>
          <div>
            <span>
              {" "}
              <h2>
                <Typical
                  loop={Infinity}
                  steps={[
                    "HTML",
                    1000,
                    "CSS",
                    1000,
                    "JAVASCRIPT",
                    1000,
                    "PHP",
                    1000,
                  ]}
                />
              </h2>
            </span>
          </div>
          <div></div>

          <div>
            <NavLink to="/Contact">
              <button className="btnHireMe">Hire Me</button>
            </NavLink>

            <a
              href="Curriculum_Vitae_Djallal Yousfi_CV.pdf"
              download="Djallal Curriculum_Vitae_Djallal Yousfi_CV.pdf"
            >
              <button className="buttonResume">Get Resume</button> <br /> <br />
            </a>
          </div>
        </div>
        <div className="image-background">
          <div className="imageContainer">
            <img src={image} alt="PhotoPortfolio" className="profile-image" />
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
