import React from "react";
import { NavLink } from "react-router-dom";
import "./Header.modules.css";

function Header() {
  return (
    <nav>
      <header className="header">
        <ul className="header-list">
          <li className="nom">Djallal Yousfi</li>
        </ul>
        <ul className="header-list">
          <li>
            <NavLink to="/">Home</NavLink>
          </li>
          <li>
            <NavLink to="/AboutMe">About</NavLink>
          </li>
          <li>
            <NavLink to="/Formation">Formation</NavLink>
          </li>
          <li>
            <NavLink to="/Projects">Projects</NavLink>
          </li>
          <li>
            <NavLink to="/Contact">Contact</NavLink>
          </li>
          <li>
            <NavLink to="/Testimonials">Testimonials</NavLink>
          </li>
        </ul>
      </header>
    </nav>
  );
}

export default Header;
