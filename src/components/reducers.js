const initialState = {
  testimonials: [],
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_TESTIMONIAL":
      return {
        ...state,
        testimonials: [...state.testimonials, action.payload],
      };
    case "DELETE_TESTIMONIAL":
      return {
        ...state,
        testimonials: state.testimonials.filter(
          (testimonial, index) => index !== action.payload
        ),
      };
    case "UPDATE_TESTIMONIAL":
      return {
        ...state,
        testimonials: state.testimonials.map((testimonial, index) =>
          index === action.payload.index
            ? action.payload.testimonial
            : testimonial
        ),
      };
    default:
      return state;
  }
};

export default rootReducer;
