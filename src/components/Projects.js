import React from "react";
import "./Projects.css";
import image from "../Images/imageProj.png";
import { NavLink } from "react-router-dom";

function Projects() {
  return (
    <div className="projects">
      <h2>Projects</h2>
      <div className="projets-12">
        <div className="projet1">
          <ul>
            <li>
              <img src={image} alt="Projet1" className="img-projet1" />
              <p>2020</p>
              <h2>Application web E-learning</h2>
              <p>
                L'objectif principal de ce projet est de concevoir et de
                développer une application web pour l'apprentissage en ligne, en
                utilisant des outils et des frameworks tels que Laravel et
                Bootstrap. À l'échelle mondiale, la technologie et
                l'enseignement à distance ont pris une importance considérable,
                ce qui nous a inspiré pour créer une plateforme d'apprentissage.
                Cette plateforme permettra aux apprenants et aux enseignants
                d'accomplir leur travail habituel depuis leur domicile,
                technologies: "Laraval, Bootstrap
              </p>
            </li>
            <li>
              <NavLink to="/firstproject">
                <button className="btnseemore1">See More ...</button>
              </NavLink>
            </li>
          </ul>
        </div>

        <div className="projet2">
          <ul>
            <li>
              <img src={image} alt="Projet1" className="img-projet1" />
              <p>2023</p>
              <h2>
                Mise en place d'une plateforme d'envoi d'objets à l'étranger
                Co-Luggage
              </h2>
              <p>
                Le projet vise à simplifier l'envoi international d'objets en
                mettant en relation les voyageurs et les expéditeurs. Notre
                objectif est de fournir une solution pratique et économique qui
                offre une alternative aux services d'expédition traditionnels.
                Nous voulons rendre le processus d'expédition plus accessible en
                permettant aux utilisateurs de trouver des voyageurs de
                confiance pour transporter leurs colis à l'étranger. En
                connectant les voyageurs et les expéditeurs, nous visons à
                réduire les coûts et les complications associés à l'envoi
                international, offrant ainsi une solution plus abordable et
                pratique pour tous les utilisateurs.", technologies: Node js,
                React js
              </p>
            </li>
            <li>
              <NavLink to="/secondproject">
                <button className="btnseemore1">See More ...</button>
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Projects;
